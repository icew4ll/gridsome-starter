const url = "https://spiritwalk.netlify.com";
const site = "icew4ll";
const desc = "starterdamus";
module.exports = {
  siteName: site,
  siteDescription: desc,
  siteUrl: url,
  titleTemplate: `%s | ` + site,

  templates: {
    Blog: [
      {
        path: "/:title"
      }
    ],
    CustomPage: [
      {
        path: "/:title",
        component: "~/templates/CustomPage.vue"
      }
    ],
    Category: [
      {
        path: "/category/:title",
        component: "~/templates/Category.vue"
      }
    ],
    Author: [
      {
        path: "/author/:name",
        component: "~/templates/Author.vue"
      }
    ],
    Tag: [
      {
        path: "/tags/:title",
        component: "~/templates/Tag.vue"
      }
    ]
  },

  plugins: [
    "gridsome-plugin-robots",
    {
      use: "gridsome-plugin-pwa",
      options: {
        title: "icew4ll Starter",
        description: "icew4ll starter", // Optional
        startUrl: "/",
        display: "standalone",
        gcm_sender_id: undefined,
        statusBarStyle: "default",
        manifestPath: "manifest.json",
        disableServiceWorker: false,
        serviceWorkerPath: "service-worker.js",
        cachedFileTypes: "js,json,css,html,png,jpg,jpeg,svg",
        shortName: "icew4ll starter",
        themeColor: "#000000",
        lang: "en-US",
        backgroundColor: "#000000",
        icon: "./src/favicon.png", // must be provided like 'src/favicon.png'
        msTileColor: "#000000"
      }
    },
    {
      use: "gridsome-source-static-meta",
      options: {
        path: "content/site/*.json"
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Author",
        path: "./content/author/*.md"
      }
    },
    {
      // Create posts from markdown files
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Blog",
        path: "content/blog/**/*.md",
        refs: {
          // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
          author: "Author",
          tags: {
            typeName: "Tag",
            create: true
          },
          category: {
            typeName: "Category",
            create: true
          }
        }
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "CustomPage",
        path: "./content/pages/*.md"
      }
    },
    {
      use: "gridsome-plugin-tailwindcss",
      options: {
        tailwindConfig: "./tailwind.config.js",
        purgeConfig: {
          whitelist: [
            "svg-inline--fa",
            "table",
            "table-striped",
            "table-bordered",
            "table-hover",
            "table-sm"
          ],
          whitelistPatterns: [
            /fa-$/,
            /blockquote$/,
            /code$/,
            /pre$/,
            /table$/,
            /table-$/,
            /vueperslide$/,
            /vueperslide-$/
          ]
        },
        presetEnvConfig: {},
        shouldPurge: false,
        shouldImport: true,
        shouldTimeTravel: true,
        shouldPurgeUnusedKeyframes: true
      }
    },
    {
      use: "@gridsome/plugin-sitemap",
      options: {
        cacheTime: 600000 // default
      }
    },
    {
      use: "gridsome-plugin-rss",
      options: {
        contentTypeName: "Blog",
        feedOptions: {
          title: desc,
          feed_url: url + "/rss.xml",
          site_url: url
        },
        feedItemOptions: node => ({
          title: node.title,
          excerpt: node.excerpt,
          url: url + node.path,
          author: node.author,
          created: node.created
        }),
        output: {
          dir: "./static",
          name: "rss.xml"
        }
      }
    },
    {
      use: "gridsome-plugin-flexsearch",
      options: {
        searchFields: ["title", "category", "excerpt", "content", "tags"],
        collections: [
          {
            typeName: "Blog",
            indexName: "Blog",
            fields: ["title", "category", "excerpt", "content", "tags"]
          }
        ]
      }
    }
  ],
  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: "_blank",
      externalLinksRel: ["nofollow", "noopener", "noreferrer"],
      anchorClassName: "icon icon-link",
      plugins: [
        "remark-attr",
        [
          "gridsome-plugin-remark-prismjs-all",
          {
            noInlineHighlight: false,
            showLineNumbers: false
          }
        ]
      ]
    }
  },
  chainWebpack: config => {
    config.resolve.alias.set("@pageImage", "@/assets/images");
  }
};
