#!/bin/sh

# copy project files to another project
if [ $# -ge 1 ]; then
	echo "COPYING..."
	src=~/m/gridsome-starter
	des=$1
	find "$src" -type f \( -iname "*" ! -iname "yarn-error.log" \) \
		! -path "$src/node_modules/*" \
		! -path "$src/dist/*" \
		! -path "$src/.cache/*" \
		! -path "$src/content/*" \
		! -path "$src/src/.temp/*" \
		! -path "$src/.git/*" \
		-exec sh -c '
	file={}
	base=$(basename '"$src"')
	des='"$des"'${file#*$base}
	echo "$file --> $des"
	cp $file $des
	' \;
else
	echo "destination folder was not supplied" && exit 1
fi
